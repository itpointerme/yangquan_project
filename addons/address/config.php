<?php

return array (
  0 => 
  array (
    'name' => 'location',
    'title' => '默认检索城市',
    'type' => 'string',
    'content' => 
    array (
    ),
    'value' => '山西',
    'rule' => 'required',
    'msg' => '',
    'tip' => '',
    'ok' => '',
    'extend' => '',
  ),
  1 => 
  array (
    'name' => 'scale',
    'title' => '默认缩放级别',
    'type' => 'string',
    'content' => 
    array (
    ),
    'value' => '12',
    'rule' => 'required',
    'msg' => '',
    'tip' => '',
    'ok' => '',
    'extend' => '',
  ),
  2 => 
  array (
    'name' => 'lat',
    'title' => '默认Lat',
    'type' => 'string',
    'content' => 
    array (
    ),
    'value' => '37.87059',
    'rule' => 'required',
    'msg' => '',
    'tip' => '',
    'ok' => '',
    'extend' => '',
  ),
  3 => 
  array (
    'name' => 'lng',
    'title' => '默认Lng',
    'type' => 'string',
    'content' => 
    array (
    ),
    'value' => '112.55067',
    'rule' => 'required',
    'msg' => '',
    'tip' => '',
    'ok' => '',
    'extend' => '',
  ),
);
