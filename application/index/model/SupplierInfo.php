<?php
namespace app\index\model;
use think\Model;


class SupplierInfo extends Model
{
	protected $table = 'fa_supplier_info';


	public function orderinfo()
    {
        return $this->morphOne('Order', 'orderable');
    }

}
