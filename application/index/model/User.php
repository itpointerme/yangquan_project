<?php
namespace app\index\model;
use think\Model;

class User extends Model
{
	public function Order(){

		return $this -> hasMany('Order','user_id','id');
	}
	
	public function orderinfo()
    {
        return $this->morphOne('Order', 'orderable');
    }

}