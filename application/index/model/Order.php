<?php
namespace app\index\model;
use think\Model;

class Order extends Model
{
	protected $table = 'fa_order';

	 public function orderable()
    {
        return $this->morphTo();
    }

}