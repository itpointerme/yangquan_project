<?php
namespace app\index\controller;

use app\common\controller\Frontend;
use think\Request;
use app\index\model\User;


class Check extends Frontend {

	protected $noNeedLogin = '*';
    protected $noNeedRight = '*';
    protected $layout = '';

    public function _initialize(){

    	parent::_initialize();

    	$c = request()->controller();
    	$v = request()->action();
    	$pp = strtolower($c.'/'.$v);

    	if(!session('uidCheck') && $pp !== 'check/login'){

    		$this -> redirect('index/check/login');
    	}
    }


    public function _empty(){

    	$this -> redirect('index/check/login');
    }

	public function login()
	{	

		if(request()->isAjax()){

			//$data['sf_pass']    = md5(input('pwd'));
			$data['sf_pass']    = '111111';
			$data['sf_account'] = input('uname');

			$result = Db('staff') ->where($data) -> value('id');

			if($result){
				session('uidCheck',$result);
				return $result;
			}else{
				return false;
			}
			
		}
		return $this ->fetch();
	}

	public function show()
	{	

		return $this ->fetch();
	}

	//查询用户信息
	public function info(){

		if(request()->isAjax() && input('phone')){

			$str = input('phone');

			$user = User::get(['mobile' => $str]);

			if(!$user) return null;

			$data = $user -> getData('join_actid');


			if(in_array('2',explode(',',$data))){

				return 'join';
				die;
			};

			$m = array();

			$m[0] = $user -> getData('id');;
			if($user){

				$m[1] = $user-> Order() ->sum('money');
			}else{
				$m = '';
			}

			if($m){
				
				return $m;
			}else{

				return false;
			}
		}	
	}
	//获取验证码
	public function yzm(){


		return '123456';
	}

	//记录用户已参与活动id
	public function updateact(){

		if(request()->isAjax() && input('u')){

			$actid = 2;

			$data = array();
			$data['icnum'] = input('cnum');
			$data['phone'] = input('phone');
			$data['time']  = date('Y-m-d',time());
			
			$fpath = ROOT_PATH.'/public/assets/active/active_'.$actid.'.txt';
			
			file_put_contents($fpath,json_encode($data).';'.PHP_EOL, FILE_APPEND);

			$act = '2';	

			$act_id = User::get(input('u'))-> getData('join_actid');

			$act_str = $act_id.','.$act;


			$user = new User;
			$msg = $user ->save(['join_actid'=>$act_str],['id'=>input('u')]);

			if($msg){

				return $msg;
			}else{

				return '';
			}
		}

	}
	
	//历史
	public function detail()
	{
		
		$actid = 2;

		$fpath = ROOT_PATH.'/public/assets/active/active_'.$actid.'.txt';

		$data = $this -> sehistory($fpath);
		array_pop($data);
		foreach($data as $k => $v){
			$v = json_decode($v,true);
			if($v['time'] == date('Ymd',time()))continue;
			$data[$k] = $v;
		};

		$this -> assign('data',$data);
		return $this -> fetch();
	}

	public function sephone(){

		if(request()->isAjax() && input('phone')){

			$actid = 2;
		    $fpath = ROOT_PATH.'/public/assets/active/active_'.$actid.'.txt';

			$data = $this -> sehistory($fpath);
			$phone = array();
			foreach($data as $k => $v){
				$v = json_decode($v,true);

				if($v['phone'] == input('phone')){
					$phone[] = $v;
				}

			};
			return $phone;
		}else{

			return '';
		}

	}

	//退出
	public function outlogin(){

		session('uidCheck',null);

		$this -> redirect('index/check/login');
	}

	private function sehistory($path){

		if(file_exists($path)){
			
			$data = explode(';',str_replace(PHP_EOL,'', file_get_contents($path)));
			return $data;
		}else{

			return '';
		}

	}



}