<?php
namespace app\index\controller;

use app\common\controller\Frontend;
use fast\Random;
use app\common\library\Token;


class Login extends Frontend
{

    protected $noNeedLogin = '*';
    protected $noNeedRight = '*';
    protected $layout = '';
    protected $model = null;

    public function _initialize()
    {   
        parent::_initialize();
        
        $this->model = new \app\admin\model\User;
    }

    public function index(){

        if(request()->isAjax()){

            if(!input('u') || !input('pwd')){

                return 0;die;
            }
            $salt = Db('user')->where('username',input('u'))->value('salt');

            if(!$salt){

                return 1;die;
            }

            $inf['username'] = input('u');
            $inf['password'] = md5(md5(input('pwd')).$salt);

            $jieguo = Db('user')->where($inf)->find();

            if($jieguo){
                $this->model->where('id',$jieguo['id'])->update(['logintime'=>time()]);
                session('userid',$jieguo['id']);
                session('username',$jieguo['username']);

                return 2;
            }else{

                return 0;
            }

        }

    	return $this->view->fetch();
    }


    public function register(){

        if ($this->request->isPost())
        {   
            
            $params = $this->request->post();
            if ($params)
            {   
                $params['salt']     = Random::alnum();
                $params['nickname'] = session('nick')?session('nick'):$params['username'];
                $params['password'] = md5(md5($params['password']) . $params['salt']);
                $params['avatar']   = '/assets/img/avatar.png'; //设置新管理员默认头像。
                $params['loginip']  = $params['joinip'] = request()->ip();
                $params['jointime'] = $params['createtime']  = time();
                $params['status']   = 'normal';

                $result = $this->model->save($params);
                if ($result === false)
                {
                    $this->error($this->model->getError());
                }

                $this->redirect('index');

            }
            $this->error();
        }
    	return $this->view->fetch();
    }

    public function recode(){


        return 123456;
    }

    //安全退出
    public function saveout(){

        session(null);

        return $this -> redirect('index');

    }


    public function returncode(){

        if(request()->isAjax() && input('phone')){

            return 12345;
        }
    }


    public function checktell(){

        if(request()->isAjax() && input('phone')){

            $aa = Db('user') -> where('mobile',input('phone')) -> value('id');

            if($aa){

                return $aa;   
            }
        }else{

            return 'error';
        }
    }

    public function savepwd(){

       if(request()->isAjax() && input('phone')){

            $p['mobile'] = input('phone');

            $salt = Db('user') -> where($p) -> value('salt');

            $pass['password'] = md5(md5(input('pwd')).$salt);

            $save = Db('user') -> where($p)->update($pass);

            if($save){
                return 1; 
            }else{

                return 'error';
            }

       }

    }
    
}