<?php

namespace app\index\controller;

use app\common\controller\Frontend;
use app\common\library\Token;
use app\index\model\User;


class Index extends Frontend
{

    protected $noNeedLogin = '*';
    protected $noNeedRight = '*';
    protected $layout = '';

    public function _initialize()
    {
        parent::_initialize();

        if(!session('userid')){

            return $this -> redirect('index/login/index');
        }
    }

    public function _empty(){


        return $this -> redirect('index/login/index');
    }
    //首页
    
    public function index()
    {   
       
        $db_banner = Db('banner')-> where('ban_state',1) ->order('ban_time desc') -> limit(3);
        $db_config = Db('config');
        //轮播
        //$lunbo = $db_banner -> where('ban_pos',0) ->select();

        $lunbo     = Db('config') -> where('name','index_slide') -> value('value');

        $banner    = explode(',',$lunbo);
        //广告
        $gg        = Db('config') -> where('name','ad_slide') -> value('value');

        $guanggao  = explode(',',$gg);
                
        //分类
        $cate      = Db('supplier_cat')
                   -> where('status',1)
                   -> limit(6)
                   -> select();

        //公告

        $notice    = Db('config') -> where('name','pay_notice') -> value('value');
        

        $shop = Db('supplier_info') 
              -> field('supplier_id,name,address,business_time_u,discount') 
              -> where(['status'=>1]) 
              -> select();

        
        $this -> assign('banner',$banner);
        $this -> assign('cate',$cate);
        $this -> assign('notice',$notice);
        $this -> assign('guanggao',$guanggao);
        $this -> assign('shop',$shop);
        

        return $this->view->fetch();
    }

        // 活动
   
    public function activity()
    {


        return $this->view->fetch();
    }


        // 领券
    public function voucher()
    {


        return $this->view->fetch();
    }


            // 扫码
    public function scan()
    {


        return $this->view->fetch();
    }





        // 商家
    public function shop()
    {

        $shop = Db('supplier_info') 
              -> field('supplier_id,name,address,business_time_u,discount,cat_id') 
              -> where(['status'=>1]) 
              -> select();

        $cate = Db('supplier_cat') 
              -> where('status',1) 
              -> limit(6)
              -> column('id,name');

        

        $this -> assign('shop',$shop);
        $this -> assign('cate',$cate);

        return $this->view->fetch();
    }

    // 商家详情
    public function shopinfo()
    {

        if(input('?get.id')){
            $shopinfo = Db('supplier_info')  
                        -> where(['status'=>1,'supplier_id'=>input('id')]) 
                        -> find();
        }else{
            $this -> back();
        }

        $this -> assign('data',$shopinfo);
        return $this->view->fetch();
    }

        //商家入驻
    public function seller(){


        return $this->view->fetch();
    }



        // 我的
    public function personal()
    {
        $data = User::get(session('userid')) -> toArray();

        $active = Db('active') -> where('status',1) -> select();

        $this -> assign('info',$data);
        $this -> assign('active',$active);
        return $this->view->fetch();
    }


    //绑定手机号
    public function bindmobile(){

         return $this->view->fetch();
    }

    //我的卡券
    public function coupon(){

         return $this->view->fetch();
    }

    //消费明细
    public function spend(){

        $user = User::get(session('userid'));

        $zz   = $user -> orderinfo();

     

        $data = $user -> Order() -> select();

        $this -> assign('data',$data);
        return $this->view->fetch();
    }

    //活动详情
    public function actinfo(){

        if(request()->isGet()){

            $info = Db('active') -> where('id',input('id')) -> find();  

            $this -> assign('info',$info);
            return $this->view->fetch();
        }


        return $this->back();
    }


    //关于我们
    public function aboutus(){


        return $this->view->fetch();
    }

        //公交卡
    // public function bus(){


    //     return $this->view->fetch();
    // }
    
    /*商家入驻*/
    public function ajaxShop(){

        if(request()->isAjax()){

            $data = input();


            $only = Db('supplier_info')
                  ->where(['name' => $data['name']])
                  ->find();

            if($only)return 'repeat';

           

            if(!empty($_FILES["zhizhao"])){    

                $path = ROOT_PATH. '/public/uploads/' .date("Ymd",time());

                if (!is_dir($path)){ 
                  mkdir($path);
                  chmod($path, 0777);
                }

                $Url = $path.'/'.time()."_".rand(1111,9999);

                if(!move_uploaded_file($_FILES["zhizhao"]['tmp_name'], $Url.'.jpg')) die('图片上传失败');

                $data['zhizhao'] = $Url.'.jpg';

            }
            $data['status'] = 0;
            $data['createtime'] = time();
            $id = Db('supplier_info') -> insertGetId($data);

            if($id){

                return 1; 
            }else{
                return 0;
            }
        }


    }

}
