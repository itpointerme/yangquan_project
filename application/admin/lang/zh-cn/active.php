<?php

return [
    'Id'  =>  '活动ID',
    'Title'  =>  '活动标题',
    'Desc'  =>  '活动描述',
    'Address'  =>  '活动地址',
    'Createtime'  =>  '创建时间',
    'Endtime'  =>  '结束时间',
    'Integral' => '积分（10-20）',
    'Growth' => '成长值（10-20）'
];
