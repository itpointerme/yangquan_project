<?php

return [
    'Id'  =>  '员工ID',
    'Supplier_id'  =>  '商户ID',
    'Sf_account'  =>  '账号',
    'Sf_pass'  =>  '密码',
    'Openid'  =>  'openid',
    'Nick_name'  =>  '微信昵称',
    'Real_name'  =>  '真实姓名',
    'Phone'  =>  '手机号',
    'Createtime'  =>  '创建时间',
    'Online'  =>  '上线时间',
    'Outline'  =>  '下线时间',
    'Done_num'  =>  '成交笔数',
    'Done_money'  =>  '成交金额',
    'Status'  =>  '在线状态',
    'Updatetime'  =>  '更新时间'
];
