<?php

return [
    'Id'  =>  '卡券ID',
    'Title'  =>  '标题',
    'Desc'  =>  '描述',
    'Agio'  =>  '折扣比例',
    'Createtime'  =>  '创建时间'
];
