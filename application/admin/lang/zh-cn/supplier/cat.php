<?php

return [
    'Id'  =>  '分类ID',
    'Name'  =>  '分类名称',
    'Img'  =>  '图标',
    'Status'  =>  '是否启用',
    'Createtime'  =>  '创建时间',
    'Updatetime'  =>  '更新时间'
];
