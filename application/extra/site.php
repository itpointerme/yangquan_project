<?php

return array (
  'name' => '阳泉返利网',
  'beian' => '',
  'cdnurl' => '',
  'version' => '1.0.1',
  'timezone' => 'Asia/Shanghai',
  'forbiddenip' => '',
  'languages' => 
  array (
    'backend' => 'zh-cn',
    'frontend' => 'zh-cn',
  ),
  'fixedpage' => 'dashboard',
  'categorytype' => 
  array (
    'default' => 'Default',
    'page' => 'Page',
    'article' => 'Article',
    'test' => 'Test',
  ),
  'configgroup' => 
  array (
    'basic' => 'Basic',
    'email' => 'Email',
    'dictionary' => 'Dictionary',
    'notice' => 'Notice',
    'slide' => 'Slide',
    'other' => 'Other',
  ),
  'mail_type' => '1',
  'mail_smtp_host' => 'smtp.qq.com',
  'mail_smtp_port' => '465',
  'mail_smtp_user' => '10000',
  'mail_smtp_pass' => 'password',
  'mail_verify_type' => '2',
  'mail_from' => '10000@qq.com',
  'slide' => 
  array (
    0 => '',
  ),
  'pay_notice' => '双12任意店铺消费满300元，减100元啦',
  'supplier_notice' => '',
  'index_slide' => 
  array (
    0 => '/uploads/20181206/8117d5403bd5d80ccb9e2b89c62262ec.jpg',
    1 => '/uploads/20190121/745860eac3d445c98b1b1859a5124fab.jpg',
    2 => '/uploads/20190121/9601d3db9e47c156d198b7cd88dc9ca3.jpg',
  ),
  'ad_slide' => 
  array (
    0 => '/uploads/20190122/3b3e29066c85fab0253db0e9270e8737.jpg',
    1 => '/uploads/20190122/ea8c57880fe7916fb3c9b8d429f22e52.jpg',
  ),
  'ban' => '',
  'cycle' => '',
  'phone' => '',
);