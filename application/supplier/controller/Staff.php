<?php

namespace app\supplier\controller;

use app\common\controller\Backend;
use app\admin\model\supplier\Info;
use app\admin\model\Admin;

/**
 * 商家收银员管理
 *
 * @icon fa fa-circle-o
 */
class Staff extends Backend
{
    
    /**
     * Staff模型对象
     * @var \app\admin\model\Staff
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\Staff;
    }

    /**
    * 员工管理显示
    * @author 太原大勇哥
    */
     public function index()
    {
        if ($this->request->isAjax()) {
            if($this->auth->id == 1 or $this->auth->id == 8){
                $list = $this->model->select();
            }else{
                $list = $this->model->where('supplier_id',$this->auth->id)->select();
            }

            $list = collection($list)->toArray();
            $total = count($list);
            $result = array("total" => $total, "rows" => $list);
            return json($result);
        }
        return $this->view->fetch();
    }


    public function add()
    {
        if ($this->request->isPost())
        {
            $params = $this->request->post('row/a');
           // halt($params);
            if ($params)
            {
               // halt($params);
                $this->model->create($params);
                $this->success();
            }
            $this->error();
        }
        $this->getSupplierInfo();
        return $this->view->fetch();

    }


     /**
    * 编辑员工信息
    * @author 太原大勇哥
    */
    function edit($ids = NULL)
    {
        $row = $this->model->get(['id' => $ids]);
        if (!$row)
            $this->error(__('No Results were found'));
        if ($this->request->isPost())
        {
            $params = $this->request->post("row/a");
            if ($params)
            {
                $result = $row->save($params);
                if ($result === FALSE)
                {
                    $this->error($row->getError());
                }
                $this->success();
            }
            $this->error();
        }
        $this->getSupplierInfo();
        $this->view->assign("row", $row);
        return $this->view->fetch();
    }

    /**
    * 获取商户信息
    * @author 太原大勇哥
    */
    private function getSupplierInfo()
    {
        //获取所有权限等于商家的名称
        $suppliers = collection(Admin::alias('a')->join('fa_auth_group_access b', 'a.id = b.uid', 'LEFT')->where('b.group_id',10)->select())->toArray();
        $list = Info::with('getsupplier')->select();

        $this->assign('suppliers', $suppliers);
        //halt(collection($list)->toArray());
        $this->assign('list', $list);
    }
}
