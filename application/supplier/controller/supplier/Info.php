<?php

namespace app\supplier\controller\supplier;

use app\common\controller\Backend;
use app\admin\model\supplier\Cat;
use app\admin\model\Admin;

/**
 * 商家信息管理
 *
 * @icon fa fa-circle-o
 * @author 太原大勇哥
 */
class Info extends Backend
{
    
    /**
     * Info模型对象
     * @var \app\admin\model\supplier\Info
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\supplier\Info;

    }


    public function index()
    {
        if ($this->request->isAjax())
        {
            if($this->auth->id == 1 or $this->auth->id == 8){
                $list = $this->model->select();
            }else{
                $list = $this->model->where('admin_id',$this->auth->id)->select();
            }

            $list = collection($list)->toArray();
            $total = count($list);
            $result = array("total" => $total, "rows" => $list);
            return json($result);
        }
        return $this->view->fetch();
    }

    
    /**
     * 添加商家信息
     * @author 太原大勇哥
     */
    public function add()
    {

        if ($this->request->isPost())
        {
            $params = $this->request->post('row/a');
           // halt($params);
            if ($params)
            {
               // halt($params);
                $this->model->create($params);
                $this->success();
            }
            $this->error();
        }
        //查询商户信息
        $adminlist = \think\Db::table('fa_admin')->where(function($query){
                        $query->table('fa_supplier_info')->field('fa_supplier_info.admin_id')->where(function($query){
                        $query->where('fa_supplier_info.admin_id = fa_admin.id');
                    });
        },'notexists')->where('fa_admin.isdel','<>',1)->select();
        $this->assign('adminlist',$adminlist);
        $this->lodingOther();
        return $this->view->fetch();
    }

    /**
    * 编辑商家信息
    * @author 太原大勇哥
    */
    function edit($ids = NULL)
    {
        $row = $this->model->get(['supplier_id' => $ids]);
        if (!$row)
            $this->error(__('No Results were found'));
        if ($this->request->isPost())
        {
            $params = $this->request->post("row/a");
            if ($params)
            {
                $result = $row->save($params);
                if ($result === FALSE)
                {
                    $this->error($row->getError());
                }
                $this->success();
            }
            $this->error();
        }
        //查出当前用户是否为商家
        $adminlist = Admin::alias('a')->join('fa_auth_group_access b', 'a.id = b.uid', 'LEFT')->where('b.group_id',10)->where('a.id',$this->auth->id)->select();
        if(!$adminlist)
            $adminlist = Admin::where('fa_admin.isdel','<>',1)->select();
        
        //halt($adminlist);

        $this->assign('adminlist',$adminlist);
        $this->lodingOther();
        $this->view->assign("row", $row);
        return $this->view->fetch();
    }

    /**
    * 自定义地图
    * @author 太原大勇哥
    */
    private function lodingOther()
    {
        $config = get_addon_config('address');
        $lat = $this->request->get('lat', $config['lat']);
        $lng = $this->request->get('lng', $config['lng']);
        $catlist = Cat::field('id,name,status')->where('status',1)->select();
        $this->assign('lat', $lat);
        $this->assign('lng', $lng);
        $this->assign('catlist',$catlist);
        $this->assign('scale', $config['scale']);
        $this->assign('location', $config['location']);
    }

}
