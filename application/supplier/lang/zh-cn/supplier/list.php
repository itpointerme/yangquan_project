<?php

return [
    'Sp_id'  =>  '商家id',
    'Sp_name'  =>  '商户账号',
    'Sp_account'  =>  '密码',
    'Sp_pass'  =>  '店铺名称',
    'Sp_major'  =>  '主营',
    'Sp_time'  =>  '营业时间',
    'Sp_adr'  =>  '地址',
    'Sp_info'  =>  '详情',
    'Sp_bg'  =>  '背景图',
    'Sp_money'  =>  '金额',
    'Sp_point'  =>  '积分',
    'Sp_total'  =>  '累计提现',
    'Sp_cash_pwd'  =>  '资金密码',
    'Sp_phone'  =>  '手机号',
    'Sp_bank'  =>  '银行',
    'Sp_bank_num'  =>  '卡号',
    'Sp_bank_sup'  =>  '开户支行',
    'Sp_bank_name'  =>  '户名',
    'Sp_status'  =>  '是否开启',
    'Sp_cat_id'  =>  '商家分类',
    'Cover'     => '商家封面'
];
