<?php

namespace app\admin\model\supplier;

use think\Model;
use app\admin\model\Admin;

class Info extends Model
{
    // 表名
    protected $name = 'supplier_info';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    
    // 追加属性
    protected $append = [

    ];
    

    public function getsupplier()
    {
        return $this->hasOne(Admin::class, 'id', 'admin_id');
    }

}
