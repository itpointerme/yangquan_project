define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'supplier/info/index',
                    add_url: 'supplier/info/add',
                    edit_url: 'supplier/info/edit',
                    del_url: 'supplier/info/del',
                    multi_url: 'supplier/info/multi',
                    table: 'supplier_info',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'supplier_id',
                sortName: 'supplier_id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'supplier_id', title: __('Supplier_id')},
                        {field: 'name', title: __('Name')},
                        {field: 'major', title: __('Major')},
                        /*{field: 'business_time_u', title: __('Business_time'), operate:'RANGE', addclass:'datetimerange'},
                        {field: 'business_time_d', title: __('Business_time'), operate:'RANGE', addclass:'datetimerange'},*/
                        {field: 'address', title: __('Address')},
                        /*{field: 'info', title: __('Info')},*/
                        {field: 'bg_img', title: __('Bg_img')},
                        {field: 'money', title: __('Money')},
                        {field: 'point', title: __('Point')},
                        /*{field: 'cash_withdrawal', title: __('Cash_withdrawal')},
                        {field: 'fund password', title: __('Fund password')},
                        {field: 'phone', title: __('Phone')},
                        {field: 'bank', title: __('Bank')},
                        {field: 'bank_num', title: __('Bank_num')},
                        {field: 'bank_sup', title: __('Bank_sup')},
                        {field: 'bank_name', title: __('Bank_name')},
                        {field: 'status', title: __('Status')},
                        {field: 'cat_id', title: __('Cat_id')},
                        {field: 'order', title: __('Order')},
                        {field: 'createtime', title: __('Createtime'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'updatetime', title: __('Updatetime'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'take_rate', title: __('Take_rate')},
                        {field: 'pay_point_rate', title: __('Pay_point_rate')},
                        {field: 'red_pack_rate', title: __('Red_pack_rate')},
                        {field: 'discount', title: __('Discount')},
                        {field: 'change_point_rate', title: __('Change_point_rate')},
                        {field: 'rebate_cash', title: __('Rebate_cash')},*/
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});