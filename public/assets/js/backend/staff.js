define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'staff/index',
                    add_url: 'staff/add',
                    edit_url: 'staff/edit',
                    del_url: 'staff/del',
                    multi_url: 'staff/multi',
                    table: 'staff',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        /*{field: 'supplier_id', title: __('Supplier_id')},*/
                        {field: 'sf_account', title: __('Sf_account')},
                        {field: 'sf_pass', title: __('Sf_pass')},
                        {field: 'openid', title: __('Openid')},
                        {field: 'nick_name', title: __('Nick_name')},
                        {field: 'real_name', title: __('Real_name')},
                        {field: 'phone', title: __('Phone')},
                        /*{field: 'createtime', title: __('Createtime'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},*/
                        {field: 'online', title: __('Online'), operate:'RANGE', addclass:'datetimerange'},
                        {field: 'outline', title: __('Outline'), operate:'RANGE', addclass:'datetimerange'},
                       /* {field: 'done_num', title: __('Done_num')},
                        {field: 'done_money', title: __('Done_money')},*/
                       /* {field: 'status', title: __('Status')},
                        {field: 'updatetime', title: __('Updatetime'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},*/
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});